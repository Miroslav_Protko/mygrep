#!/usr/bin/env python2.7
# -*- codding: utf-8 -*-

import argparse
import os
from grep.config_tools import create_config, load_config, update_config
from grep.operation_in_file import recursive_search, search_in_file


CONFIG_PATH = os.path.expanduser('~/mygrep/config.json')


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('pattern', help='Pattern to search')
    parser.add_argument('-r', action='store_true', dest='recursive', help='Recursive search')
    parser.add_argument('-o', action='store_true', dest='only_matching', help='Output only matching')
    parser.add_argument('-c', action='store_true', dest='count', help='Output number of lines found')
    parser.add_argument('-v', action='store_true', dest='invert_match', help='Output inverse search')
    parser.add_argument('-l', action='store_true', dest='files_with_matches',
                        help='Output only files name with matches')
    parser.add_argument('-update', action='store_true', dest='update_config', help='Update config file')
    parser.add_argument('-create', action='store_true', dest='create_config', help='Create config file')
    parser.add_argument('-config', default=CONFIG_PATH, help='Specifies the path to the config file in json format')
    parser.add_argument('filename', nargs='*', help='Path to search')

    return parser.parse_args()


def main():
    args = vars(parse())

    config = load_config(args['config'])

    if not os.path.exists(CONFIG_PATH):
        create_config(args['config'], args)

    if args['create_config']:
        create_config(args['config'], args)

    if args['update_config']:
        update_config(args['config'], args)

    if args['filename']:
        for arg in args['filename']:
            search_in_file(arg, args['pattern'], args['only_matching'], args['files_with_matches'],
                           args['invert_match'], args['count'])

    if args['recursive']:
        for arg in args['filename']:
            recursive_search(arg,  args['pattern'])


if __name__ == "__main__":
    main()
