#!/usr/bin/env python2.7
# -*- codding: utf-8 -*-

import re
import os


def recursive_search(path, pattern):

    found_pathes = []

    for _file in os.listdir(path):
        if os.path.isfile(os.path.join(path, _file)):
            found_pathes.append(os.path.join(path, _file))
        else:
            found_pathes.extend(recursive_search(os.path.join(path, _file), pattern))

    for path_file in found_pathes:
        search_in_file(path_file, pattern)

    return found_pathes


def search_in_file(path, pattern, only_matching=False, only_filename=False, invert=False, count=False):

    found_line = []

    count_line = 0

    if os.path.isdir(path):
        pass

    else:
        with open(path, "r") as file:

            for line in file:
                result = re.findall(pattern, line)

                if result and not invert:
                    found_line.append((path,line, result))
                    count_line = count_line + 1

                elif invert and not result:
                    found_line.append((path, line, result))

        if count:
            print count_line

        elif only_matching:
            for arg in found_line:
                print arg[2]

        elif only_filename:
            for arg in found_line:
                print arg[0]

        else:
            for arg in found_line:
                print arg[0], arg[1]

    return found_line