#Description

Console utility to search for string containing a user-defined pattern.


###Installation
Run:
``` bash 
sudo ./setup.py install
```

To uninstall:
``` bash 
pip uninstall mygrep
```

### Default config file

JSON:

{"count": false, "invert_match": false, "recursive": false, "files_with_matches": false, "pattern": "create_config", "filename": [], "update_config": false, "config": "/home/miroslav/mygrep/config.json", "create_config": false, "only_matching": false}


###Plan
* Search string with defined pattern.
* Add flags -v, -l, -w and ect.
* Add parallel and recursive round in dir. Add tests, installetion with setup.
* Web-interface.


